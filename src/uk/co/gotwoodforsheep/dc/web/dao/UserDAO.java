/**
 * 
 */
package uk.co.gotwoodforsheep.dc.web.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.oauth2.provider.BaseClientDetails;
import org.springframework.stereotype.Repository;

import uk.co.gotwoodforsheep.dc.web.bean.security.Privilege;
import uk.co.gotwoodforsheep.dc.web.bean.security.UserDetails;
import uk.co.gotwoodforsheep.dc.web.dao.rowmapper.PrivilegesRowMapper;
import uk.co.gotwoodforsheep.dc.web.dao.rowmapper.UserRowMapper;
import uk.co.gotwoodforsheep.dc.web.entity.Users;

/**
 * @author Sean
 *
 */
@Repository
public class UserDAO extends AbstractDAO {
	
	private static final String USER_BY_USERNAME = "select id, username, password, enabled from users where username = ?";
	
	private static final String PRIVILEGES_BY_USERNAME = "select p.id, p.name, p.description " +
			"from privileges p " +
			"join group_privileges gp on p.id = gp.privilege_id " +
			"join groups g on gp.group_id = g.id " +
			"join user_groups ug on g.id = ug.group_id " +
			"join users u on u.id = ug.user_id " +
			"where u.username = ?";
	
	@Autowired
	public UserDAO(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate);
	}

	public UserDetails loadUserByUsername(String username) {
		List<Users> users = getJdbcTemplate().query(USER_BY_USERNAME, new Object[] {username}, new UserRowMapper());
		if (users.isEmpty() || users.size() > 1) {
			return null;
		}
		Users user = users.get(0);
		List<Privilege> privileges = getJdbcTemplate().query(PRIVILEGES_BY_USERNAME, new Object[] {username}, new PrivilegesRowMapper());
		
		UserDetails userDetails = new UserDetails(user.getUsername(), user.getPassword(), user.isEnabled(), privileges);
		return userDetails;
	}
}
