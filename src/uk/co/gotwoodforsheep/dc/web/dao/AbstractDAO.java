/**
 * 
 */
package uk.co.gotwoodforsheep.dc.web.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

/**
 * @author Sean
 *
 */
public abstract class AbstractDAO extends NamedParameterJdbcDaoSupport {
	
	public AbstractDAO(JdbcTemplate jdbcTemplate) {
		setJdbcTemplate(jdbcTemplate);
	}
}
