package uk.co.gotwoodforsheep.dc.web.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import uk.co.gotwoodforsheep.dc.web.bean.security.Privilege;

public class PrivilegesRowMapper implements RowMapper<Privilege> {

	@Override
	public Privilege mapRow(ResultSet rs, int row) throws SQLException {
		Privilege p = new Privilege(rs.getLong("id"), rs.getString("name"), rs.getString("description"));
		return p;
	}

}
