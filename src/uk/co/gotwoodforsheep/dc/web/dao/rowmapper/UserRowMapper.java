package uk.co.gotwoodforsheep.dc.web.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import uk.co.gotwoodforsheep.dc.web.entity.Users;

public class UserRowMapper implements RowMapper<Users> {

	@Override
	public Users mapRow(ResultSet rs, int row) throws SQLException {
		Users u = new Users();
		u.setId(rs.getLong("id"));
		u.setUsername(rs.getString("username"));
		u.setPassword(rs.getString("password"));
		u.setEnabled(rs.getBoolean("enabled"));
		
		return u;
	}

}
