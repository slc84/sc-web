package uk.co.gotwoodforsheep.dc.web.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import uk.co.gotwoodforsheep.dc.web.exception.WebApplicationException;

@Repository
public class ApplicationDAO extends AbstractDAO {

	private static final Logger LOG = LoggerFactory
			.getLogger(ApplicationDAO.class);

	@Value("${application.tables.sql.file}")
	private File tableSqlFile;

	@Value("${application.data.sql.file}")
	private File staticDataSqlFile;

	@Autowired
	public ApplicationDAO(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate);
	}

	@Override
	protected void checkDaoConfig() throws IllegalArgumentException {

	}

	private boolean isDBInitialised() {
		Long tableCount = getTableCount();
		return tableCount > 0;
	}

	public Long getTableCount() {
		Long tableCount = getJdbcTemplate()
				.queryForObject(
						"SELECT COUNT(*) from information_schema.tables WHERE table_schema = 'iqdgyixc_sc-web' ",
						Long.class);

		LOG.debug("Tables found = " + tableCount);
		return tableCount;
	}

	private void initialiseDb() {
		// load tables
		String tableSql = getSqlString(tableSqlFile);
		getJdbcTemplate().batchUpdate(tableSql.split("--"));
		// load data
		String dataSql = getSqlString(staticDataSqlFile);
		getJdbcTemplate().batchUpdate(dataSql.split("--"));
	}

	private String getSqlString(File f) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(f));
			String line = null;
			StringBuilder stringBuilder = new StringBuilder();
			String ls = System.getProperty("line.separator");

			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}
			return stringBuilder.toString();
		} catch (Exception e) {
			throw new WebApplicationException("Error while reading SQL file: "
					+ f, e);
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				// meh
			}
		}
	}

	@PostConstruct
	protected void postConstruct() {
		LOG.info("Checking if DB needs initialised");
		if (!isDBInitialised()) {
			LOG.info("DB is not initialised, initialising...");
			initialiseDb();
		} else {
			LOG.info("DB is already initialised...");
		}
		LOG.info("DB table count: " + getTableCount());
	}

	public File getTableSqlFile() {
		return tableSqlFile;
	}

	public void setTableSqlFile(File tableSqlFile) {
		this.tableSqlFile = tableSqlFile;
	}
}
