/**
 * 
 */
package uk.co.gotwoodforsheep.dc.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;

import uk.co.gotwoodforsheep.dc.web.dao.UserDAO;

/**
 * @author Sean
 *
 */
@Service("userService")
public class UserService implements UserDetailsService, AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

	@Autowired
	private UserDAO userDAO;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserDetails user = userDAO.loadUserByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Username [" + username + "] not found");
		}
		return user;
	}

	@Override
	public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken preAuth) throws UsernameNotFoundException {
		return loadUserByUsername(((UserDetails)preAuth.getPrincipal()).getUsername());
	}

}
