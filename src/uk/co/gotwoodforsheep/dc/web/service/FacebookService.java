package uk.co.gotwoodforsheep.dc.web.service;

import org.springframework.web.client.RestOperations;

public class FacebookService {

	private RestOperations facebookRestTemplate;

	public String getDetails() {
		return facebookRestTemplate.getForObject("https://graph.facebook.com/me", String.class);
	}

	public void setFacebookRestTemplate(RestOperations facebookRestTemplate) {
		this.facebookRestTemplate = facebookRestTemplate;
	}
}
