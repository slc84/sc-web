package uk.co.gotwoodforsheep.dc.web.service;

import org.springframework.web.client.RestOperations;

public class GoogleService {

	private RestOperations googleRestTemplate;
	
	public String getDetails() {
		return googleRestTemplate.getForObject("https://www.googleapis.com/oauth2/v1/userinfo?alt=json", String.class);
	}
	
	public void setGoogleRestTemplate(RestOperations googleRestTemplate) {
		this.googleRestTemplate = googleRestTemplate;
	}
}
