package uk.co.gotwoodforsheep.dc.web.exception;

public class WebApplicationException extends RuntimeException {

	public WebApplicationException() {
		super();
	}

	public WebApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	public WebApplicationException(String message) {
		super(message);
	}

	public WebApplicationException(Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1202619061263525947L;

}
