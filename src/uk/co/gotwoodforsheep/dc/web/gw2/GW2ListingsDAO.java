package uk.co.gotwoodforsheep.dc.web.gw2;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import uk.co.gotwoodforsheep.dc.web.dao.AbstractDAO;

@Repository
public class GW2ListingsDAO extends AbstractDAO {

	@Autowired
	public GW2ListingsDAO(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate);
	}

	public void store(Item item) {
		// find out if it exists in item db
		List<Item> loadedItems = getJdbcTemplate().query("select * from item where data_id = ?", new ItemRowMapper(), item.getDataId());
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(item.getDescription());
		parameters.add(item.getImgUrl());
		parameters.add(item.getLevel());
		parameters.add(item.getName());
		parameters.add(item.getRarity());
		parameters.add(item.getRarityWord());
		parameters.add(item.getTypeId());
		parameters.add(item.getVendorPrice());
		
		if (loadedItems.isEmpty()) {
			parameters.add(0, item.getDataId());
			getJdbcTemplate().update("insert into item (data_id,description,img_url,level,name,rarity,rarity_word,type_id,vendor_price) values (?,?,?,?,?,?,?,?,?)", parameters.toArray());
		} else {
			//parameters.add(item.getDataId());
			//getJdbcTemplate().update("update item set (description=?,img_url=?,level=?,name=?,rarity=?,rarity_word=?,type_id=?,vendor_price=?) where data_id = ?", parameters.toArray());
		}
	}

	public void store(ItemListingHistory itemListingHistory) {
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(itemListingHistory.getDataId());
		parameters.add(itemListingHistory.getDate());
		parameters.add(itemListingHistory.getSellPrice());
		parameters.add(itemListingHistory.getSellCount());
		parameters.add(itemListingHistory.getBuyPrice());
		parameters.add(itemListingHistory.getBuyCount());
		
		getJdbcTemplate().update("insert into item_listing_history (data_id,date,sell_price,sell_count,buy_price,buy_count) values (?,?,?,?,?,?)", parameters.toArray());
	}

	public List<BuyItems> getDefiniteBuys() {
		return getJdbcTemplate().query("select i.name, i.level, i.vendor_price, ih.sell_price, ih.buy_price, CAST(ih.buy_price AS SIGNED) - CAST(ih.sell_price AS SIGNED) as tp_diff, CAST(i.vendor_price AS SIGNED) - CAST(ih.sell_price AS SIGNED) as vendor_diff, ih.date from item i left join item_listing_history ih on ih.data_id = i.data_id and date = (select max(date) from item_listing_history ih2 where ih2.data_id = i.data_id) where (ih.sell_price != 0 and ih.sell_price < i.vendor_price) or (ih.sell_price != 0 and ih.buy_price != 0 and ih.sell_price < ih.buy_price)", new BuyItemsRowMapper());
	}

	public void updateBuyAverages() {
		getJdbcTemplate().update("INSERT INTO item_listing_buy_average (data_id, average, `date`) SELECT i.data_id, avg(i.buy_price), CURRENT_TIMESTAMP() FROM item_listing_history i WHERE i.buy_price != 0 GROUP BY i.data_id ON DUPLICATE KEY UPDATE average=VALUES(average), `date`=VALUES(`date`);");
	}

	public void updateSellAverages() {
		getJdbcTemplate().update("INSERT INTO item_listing_sell_average (data_id, average, `date`) SELECT i.data_id, avg(i.sell_price), CURRENT_TIMESTAMP() FROM item_listing_history i WHERE i.sell_price != 0 GROUP BY i.data_id ON DUPLICATE KEY UPDATE average=VALUES(average), `date`=VALUES(`date`);");
	}

	public List<LowSellPriceItem> getLowSellItems() {
		return getJdbcTemplate().query("select i.name, i.level, i.vendor_price, ih.sell_price, ih.sell_count, ih.buy_price,	buy_avg.average as average_buy_price, (buy_avg.average - ih.sell_price) as avg_buy_sell_diff, ih.date from item i left join item_listing_history ih ON ih.data_id = i.data_id and date = (select max(date) from item_listing_history ih2 where ih2.data_id = i.data_id) left join item_listing_buy_average buy_avg ON i.data_id = buy_avg.data_id where (ih.sell_price != 0 and buy_avg.average != 0 and ih.sell_price < buy_avg.average and buy_avg.average - ih.sell_price > 50) order by (buy_avg.average - ih.sell_price) DESC", new BeanPropertyRowMapper<LowSellPriceItem>(LowSellPriceItem.class));
	}

	
}
