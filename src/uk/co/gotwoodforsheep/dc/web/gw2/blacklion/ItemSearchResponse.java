package uk.co.gotwoodforsheep.dc.web.gw2.blacklion;

import java.util.List;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class ItemSearchResponse {

	@SerializedName("args")
	private ItemSearchResponseArg args;
	
	@SerializedName("total")
	private String total;
	
	@SerializedName("results")
	private List<ItemSearchResponseResult> results;

	/**
	 * @return the args
	 */
	public ItemSearchResponseArg getArgs() {
		return args;
	}

	/**
	 * @param args the args to set
	 */
	public void setArgs(ItemSearchResponseArg args) {
		this.args = args;
	}

	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}

	/**
	 * @return the results
	 */
	public List<ItemSearchResponseResult> getResults() {
		return results;
	}

	/**
	 * @param results the results to set
	 */
	public void setResults(List<ItemSearchResponseResult> results) {
		this.results = results;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ItemSearchResponse [args=" + args + ", total=" + total
				+ ", results=" + results + "]";
	}


	

}
