package uk.co.gotwoodforsheep.dc.web.gw2.blacklion;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ItemSearchResponseResult {

	@SerializedName("type_id")
	private String typeId;
	
	@SerializedName("data_id")
	private String dataId;
	
	@SerializedName("name")
	private String name;
	
	@SerializedName("description")
	private String description;
	
	@SerializedName("level")
	private String level;
	
	@SerializedName("rarity")
	private String rarity;
	
	@SerializedName("vendor")
	private String vendor;
	
	@SerializedName("sell_price")
	private String sellPrice;
	
	@SerializedName("sell_count")
	private String sellCount;
	
	@SerializedName("buy_price")
	private String buyPrice;
	
	@SerializedName("buy_count")
	private String buyCount;
	
	@SerializedName("passwords")
	private List<ItemSearchResponsePassword> passwords;
	
	@SerializedName("img")
	private String img;
	
	@SerializedName("rarity_word")
	private String rarityWord;

	/**
	 * @return the typeId
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the dataId
	 */
	public String getDataId() {
		return dataId;
	}

	/**
	 * @param dataId the dataId to set
	 */
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the level
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(String level) {
		this.level = level;
	}

	/**
	 * @return the rarity
	 */
	public String getRarity() {
		return rarity;
	}

	/**
	 * @param rarity the rarity to set
	 */
	public void setRarity(String rarity) {
		this.rarity = rarity;
	}

	/**
	 * @return the vendor
	 */
	public String getVendor() {
		return vendor;
	}

	/**
	 * @param vendor the vendor to set
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	/**
	 * @return the sellPrice
	 */
	public String getSellPrice() {
		return sellPrice;
	}

	/**
	 * @param sellPrice the sellPrice to set
	 */
	public void setSellPrice(String sellPrice) {
		this.sellPrice = sellPrice;
	}

	/**
	 * @return the sellCount
	 */
	public String getSellCount() {
		return sellCount;
	}

	/**
	 * @param sellCount the sellCount to set
	 */
	public void setSellCount(String sellCount) {
		this.sellCount = sellCount;
	}

	/**
	 * @return the buyPrice
	 */
	public String getBuyPrice() {
		return buyPrice;
	}

	/**
	 * @param buyPrice the buyPrice to set
	 */
	public void setBuyPrice(String buyPrice) {
		this.buyPrice = buyPrice;
	}

	/**
	 * @return the buyCount
	 */
	public String getBuyCount() {
		return buyCount;
	}

	/**
	 * @param buyCount the buyCount to set
	 */
	public void setBuyCount(String buyCount) {
		this.buyCount = buyCount;
	}

	/**
	 * @return the passwords
	 */
	public List<ItemSearchResponsePassword> getPasswords() {
		return passwords;
	}

	/**
	 * @param passwords the passwords to set
	 */
	public void setPasswords(List<ItemSearchResponsePassword> passwords) {
		this.passwords = passwords;
	}

	/**
	 * @return the img
	 */
	public String getImg() {
		return img;
	}

	/**
	 * @param img the img to set
	 */
	public void setImg(String img) {
		this.img = img;
	}

	/**
	 * @return the rarityWord
	 */
	public String getRarityWord() {
		return rarityWord;
	}

	/**
	 * @param rarityWord the rarityWord to set
	 */
	public void setRarityWord(String rarityWord) {
		this.rarityWord = rarityWord;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ItemSearchResponseResults [typeId=" + typeId + ", dataId="
				+ dataId + ", name=" + name + ", description=" + description
				+ ", level=" + level + ", rarity=" + rarity + ", vendor="
				+ vendor + ", sellPrice=" + sellPrice + ", sellCount="
				+ sellCount + ", buyPrice=" + buyPrice + ", buyCount="
				+ buyCount + ", passwords=" + passwords + ", img=" + img
				+ ", rarityWord=" + rarityWord + "]";
	}
	
}
