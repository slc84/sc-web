package uk.co.gotwoodforsheep.dc.web.gw2.blacklion;

public class ItemSearchResponseArg {

	private Integer offset;
	private Integer count;
	private Integer levelmin;
	private Integer levelmax;
	private Integer type;
	/**
	 * @return the offset
	 */
	public Integer getOffset() {
		return offset;
	}
	/**
	 * @param offset the offset to set
	 */
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	/**
	 * @return the count
	 */
	public Integer getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(Integer count) {
		this.count = count;
	}
	/**
	 * @return the levelmin
	 */
	public Integer getLevelmin() {
		return levelmin;
	}
	/**
	 * @param levelmin the levelmin to set
	 */
	public void setLevelmin(Integer levelmin) {
		this.levelmin = levelmin;
	}
	/**
	 * @return the levelmax
	 */
	public Integer getLevelmax() {
		return levelmax;
	}
	/**
	 * @param levelmax the levelmax to set
	 */
	public void setLevelmax(Integer levelmax) {
		this.levelmax = levelmax;
	}
	/**
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ItemSearchResponseArgs [offset=" + offset + ", count=" + count
				+ ", levelmin=" + levelmin + ", levelmax=" + levelmax
				+ ", type=" + type + "]";
	}
	
	
}
