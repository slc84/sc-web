package uk.co.gotwoodforsheep.dc.web.gw2.blacklion;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class ItemSearchParameters {

	private int resultSize = 100;
	private int offset = 0;
	private int maxResults = 0;
	private String text = "";
	private int levelmin = 0;
	private int levelmax = 80;

	public void increaseOffset() {
		offset += resultSize;
	}
	
	public int getResultSize() {
		return resultSize;
	}

	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getLevelmin() {
		return levelmin;
	}

	public void setLevelmin(int levelmin) {
		this.levelmin = levelmin;
	}

	public int getLevelmax() {
		return levelmax;
	}

	public void setLevelmax(int levelmax) {
		this.levelmax = levelmax;
	}

	public List<NameValuePair> getParams() {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		addParam(params, "offset", getOffset() );
		addParam(params, "text", getText() );
		addParam(params, "levelmin", getLevelmin() );
		addParam(params, "levelmax", getLevelmax() );
		addParam(params, "count", getResultSize() );
		return params;
	}

	private void addParam(List<NameValuePair> params, String name, Object value) {
		if (value != null) {
			params.add(new BasicNameValuePair(name, value.toString()));
		}
	}

	public boolean hasMoreResults() {
		return offset < maxResults + resultSize;
	}

}
