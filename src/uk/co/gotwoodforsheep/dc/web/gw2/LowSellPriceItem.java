package uk.co.gotwoodforsheep.dc.web.gw2;

import java.util.Date;

public class LowSellPriceItem {

	private Integer dataId;
	private String name;
	private String level;
	private Integer vendorPrice;
	private Integer sellPrice;
	private Integer sellCount;
	private Integer buyPrice;
	private Integer averageBuyPrice;
	private Integer avgBuySellDiff;
	private Date date;
	public Integer getDataId() {
		return dataId;
	}
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getVendorPrice() {
		return vendorPrice;
	}
	public void setVendorPrice(Integer vendorPrice) {
		this.vendorPrice = vendorPrice;
	}
	public Integer getSellPrice() {
		return sellPrice;
	}
	public void setSellPrice(Integer sellPrice) {
		this.sellPrice = sellPrice;
	}
	public Integer getSellCount() {
		return sellCount;
	}
	public void setSellCount(Integer sellCount) {
		this.sellCount = sellCount;
	}
	public Integer getBuyPrice() {
		return buyPrice;
	}
	public void setBuyPrice(Integer buyPrice) {
		this.buyPrice = buyPrice;
	}
	public Integer getAverageBuyPrice() {
		return averageBuyPrice;
	}
	public void setAverageBuyPrice(Integer averageBuyPrice) {
		this.averageBuyPrice = averageBuyPrice;
	}
	public Integer getAvgBuySellDiff() {
		return avgBuySellDiff;
	}
	public void setAvgBuySellDiff(Integer avgBuySellDiff) {
		this.avgBuySellDiff = avgBuySellDiff;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	
}