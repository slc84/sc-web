package uk.co.gotwoodforsheep.dc.web.gw2;

import java.util.Date;

public class BuyItems {

	private String name;
	private Integer level;
	private Integer sellerPrice;
	private Integer buyerPrice;
	private Integer vendorPrice;
	private Integer tpDiff;
	private Integer vendorDiff;
	private Date date;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getSellerPrice() {
		return sellerPrice;
	}

	public void setSellerPrice(Integer sellerPrice) {
		this.sellerPrice = sellerPrice;
	}

	public Integer getBuyerPrice() {
		return buyerPrice;
	}

	public void setBuyerPrice(Integer buyerPrice) {
		this.buyerPrice = buyerPrice;
	}

	public Integer getVendorPrice() {
		return vendorPrice;
	}

	public void setVendorPrice(Integer vendorPrice) {
		this.vendorPrice = vendorPrice;
	}

	public Integer getTpDiff() {
		return tpDiff;
	}

	public void setTpDiff(Integer tpDiff) {
		this.tpDiff = tpDiff;
	}

	public Integer getVendorDiff() {
		return vendorDiff;
	}

	public void setVendorDiff(Integer vendorDiff) {
		this.vendorDiff = vendorDiff;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}



}
