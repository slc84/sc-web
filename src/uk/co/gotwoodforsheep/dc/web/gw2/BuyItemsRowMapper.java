package uk.co.gotwoodforsheep.dc.web.gw2;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


public class BuyItemsRowMapper implements RowMapper<BuyItems> {

	@Override
	public BuyItems mapRow(ResultSet rs, int row) throws SQLException {
		BuyItems buyItems = new BuyItems();
		buyItems.setBuyerPrice(rs.getInt("buy_price"));
		buyItems.setLevel(rs.getInt("level"));
		buyItems.setName(rs.getString("name"));
		buyItems.setSellerPrice(rs.getInt("sell_price"));
		buyItems.setVendorPrice(rs.getInt("vendor_price"));
		buyItems.setTpDiff(rs.getInt("tp_diff"));
		buyItems.setVendorDiff(rs.getInt("vendor_diff"));
		buyItems.setDate(rs.getDate("date"));
		return buyItems;
	}

}
