package uk.co.gotwoodforsheep.dc.web.gw2;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


public class ItemRowMapper implements RowMapper<Item> {

	@Override
	public Item mapRow(ResultSet rs, int row) throws SQLException {
		Item item = new Item();
		item.setDataId(rs.getInt("data_id"));
		item.setDescription(rs.getString("description"));
		item.setImgUrl(rs.getString("img_url"));
		item.setLevel(rs.getInt("level"));
		item.setName(rs.getString("name"));
		item.setRarity(rs.getInt("rarity"));
		item.setRarityWord(rs.getString("rarity_word"));
		item.setTypeId(rs.getInt("type_id"));
		item.setVendorPrice(rs.getInt("vendor_price"));
		return item;
	}

}
