package uk.co.gotwoodforsheep.dc.web.gw2;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import uk.co.gotwoodforsheep.dc.web.exception.WebApplicationException;
import uk.co.gotwoodforsheep.dc.web.gw2.blacklion.ItemSearchParameters;
import uk.co.gotwoodforsheep.dc.web.gw2.blacklion.ItemSearchResponse;
import uk.co.gotwoodforsheep.dc.web.gw2.blacklion.ItemSearchResponseResult;

import com.google.gson.Gson;

@Service
public class GW2Service {

	private static final Logger LOG = LoggerFactory.getLogger(GW2Service.class);

	@Value("${gw2.search-url}")
	private String gw2SearchUrl;

	@Value("${gw2.token}")
	private String token;

	@Autowired
	private HttpClientFactory httpClientFactory;

	@Autowired
	private GW2ListingsDAO gw2ListingsDAO;

	public List<ItemSearchResponseResult> getItemSearch(ItemSearchParameters searchParameters) {
		HttpClient httpclient = httpClientFactory.getHttpDefaultClient();

		List<ItemSearchResponseResult> results = new ArrayList<ItemSearchResponseResult>();
		while (searchParameters.hasMoreResults()) {

			URIBuilder uriBuilder;
			try {
				uriBuilder = new URIBuilder(gw2SearchUrl);
			} catch (URISyntaxException e) {
				throw new WebApplicationException("Error creating URL", e);
			}
			for (NameValuePair pair : searchParameters.getParams()) {
				uriBuilder.addParameter(pair.getName(), pair.getValue());
			}

			HttpGet httpGet = new HttpGet(uriBuilder.toString());
			httpGet.setHeader("Cookie", "s=" + token);

			String itemSearchJson = send(httpclient, httpGet);

			ItemSearchResponse itemSearchResponse = parseItemSearchResponse(itemSearchJson);

			searchParameters.setMaxResults(Integer.parseInt(itemSearchResponse.getTotal()));
			searchParameters.increaseOffset();
			results.addAll(itemSearchResponse.getResults());
		}
		return results;
	}

	private ItemSearchResponse parseItemSearchResponse(String itemSearchJson) {
		Gson gson = new Gson();
		return gson.fromJson(itemSearchJson, ItemSearchResponse.class);
	}

	private String send(HttpClient httpclient, HttpRequestBase request) {

		String responseString = null;

		try {
			HttpResponse response = httpclient.execute(request);
			
			// if unauthorised let's get a new cookie
			if (response.getStatusLine().getStatusCode() == 401) {
				request.releaseConnection();
				token = attemptLogin(httpclient);
				LOG.info("Reattempting request with session ID: " + token);
				request.removeHeaders("Cookie");
				request.addHeader("Cookie", "s=" + token);
				response = httpclient.execute(request);
			}
			HttpEntity entity1 = response.getEntity();
			ResponseHandler<String> handler = new BasicResponseHandler();
			responseString = handler.handleResponse(response);
			// do something useful with the response body
			// and ensure it is fully consumed
			EntityUtils.consume(entity1);
		} catch (Exception e) {
			throw new WebApplicationException("Error while sending request", e);
		} finally {
			request.releaseConnection();
		}
		return responseString;
	}

	private String attemptLogin(HttpClient httpclient) throws UnsupportedEncodingException {
		HttpPost httpPost = new HttpPost("https://account.guildwars2.com/login");
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
        nvps.add(new BasicNameValuePair("email", "sean.clark84@gmail.com"));
        nvps.add(new BasicNameValuePair("password", "Holiday777!"));
        httpPost.setEntity(new UrlEncodedFormEntity(nvps));
        httpPost.setHeader("Referer", "https://account.guildwars2.com/login");
        httpPost.getParams().setParameter(
                ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);
		send(httpclient, httpPost);
		LOG.info("Cookies: " + ((DefaultHttpClient)httpclient).getCookieStore().getCookies());
		// look for the login session id cookie
		for (Cookie c : ((DefaultHttpClient)httpclient).getCookieStore().getCookies()) {
			if ("s".equals(c.getName()) && "guildwars2.com".equals(c.getDomain())) {
				return c.getValue();
			}
		}
		throw new WebApplicationException("Unable to login to retrieve session ID");
	}

	public void updateListings(ItemSearchParameters itemSearchParameters) {
		List<ItemSearchResponseResult> itemSearchResponses = getItemSearch(new ItemSearchParameters());
		LOG.info("Retrieved " + itemSearchResponses.size() + " items.");
		for (ItemSearchResponseResult itemResult : itemSearchResponses) {
			try {
				LOG.debug("Storing item " + itemResult.getName());
				Item item = convertToItem(itemResult);
				gw2ListingsDAO.store(item);
				
				LOG.debug("Storing item history");
				ItemListingHistory itemListingHistory = convertToItemListing(itemResult);
				gw2ListingsDAO.store(itemListingHistory);

				Integer sellersPrice = Integer.parseInt(itemResult.getSellPrice());
				Integer buyersPrice = Integer.parseInt(itemResult.getBuyPrice());
				Integer merchantPrice = Integer.parseInt(itemResult.getVendor());

				if (sellersPrice != 0 && sellersPrice < merchantPrice) {
					LOG.debug("Name: " + itemResult.getName() + " Level: " + itemResult.getLevel()
							+ " Selling Price: " + sellersPrice + " Buyers Price: " + buyersPrice + " Merchant Price: "
							+ itemResult.getVendor());
				}
			} catch (Exception e) {
				LOG.error("Error processing result.", e);
			}
		}
		LOG.info("Completed storing");
	}

	private Item convertToItem(ItemSearchResponseResult itemResult) {
		Item item = new Item();
		item.setDataId(Integer.parseInt(itemResult.getDataId()));
		item.setDescription(itemResult.getDescription());
		item.setImgUrl(itemResult.getImg());
		item.setLevel(Integer.parseInt(itemResult.getLevel()));
		item.setName(itemResult.getName());
		item.setRarity(Integer.parseInt(itemResult.getRarity()));
		item.setRarityWord(itemResult.getRarityWord());
		item.setTypeId(Integer.parseInt(itemResult.getTypeId()));
		item.setVendorPrice(Integer.parseInt(itemResult.getVendor()));
		return item;
	}

	private ItemListingHistory convertToItemListing(ItemSearchResponseResult itemResult) {
		ItemListingHistory itemListingHistory = new ItemListingHistory();
		itemListingHistory.setBuyCount(Integer.parseInt(itemResult.getBuyCount()));
		itemListingHistory.setBuyPrice(Integer.parseInt(itemResult.getBuyPrice()));
		itemListingHistory.setDataId(Integer.parseInt(itemResult.getDataId()));
		itemListingHistory.setDate(new Timestamp(new Date().getTime()));
		itemListingHistory.setSellCount(Integer.parseInt(itemResult.getSellCount()));
		itemListingHistory.setSellPrice(Integer.parseInt(itemResult.getSellPrice()));
		return itemListingHistory;
	}

	public List<BuyItems> getDefiniteBuys() {
		return gw2ListingsDAO.getDefiniteBuys();
	}

	public void updateBuyAverages() {
		gw2ListingsDAO.updateBuyAverages();
	}

	public void updateSellAverages() {
		gw2ListingsDAO.updateSellAverages();
	}

	public List<LowSellPriceItem> getLowSellItems() {
		return gw2ListingsDAO.getLowSellItems();
	}

}
