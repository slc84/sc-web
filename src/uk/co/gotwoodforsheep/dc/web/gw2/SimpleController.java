package uk.co.gotwoodforsheep.dc.web.gw2;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class SimpleController {

	@Autowired
	private GW2Service gw2Service;
	
	@RequestMapping("/gw2")
	public String showValue(ModelMap modelMap) {
		List<BuyItems> buyItems = gw2Service.getDefiniteBuys();
		List<LowSellPriceItem> lowSellItems = gw2Service.getLowSellItems();
		modelMap.put("buys", buyItems);
		modelMap.put("lowSellingItems", lowSellItems);
		return "gw2/main";
	}
}
