package uk.co.gotwoodforsheep.dc.web.gw2;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import uk.co.gotwoodforsheep.dc.web.gw2.blacklion.ItemSearchParameters;
import uk.co.gotwoodforsheep.dc.web.gw2.blacklion.ItemSearchResponse;
import uk.co.gotwoodforsheep.dc.web.gw2.blacklion.ItemSearchResponseResult;

/**
 * No longer in use.
 * @author Sean
 *
 */
//@Component
public class ItemUpdater {

	private static final Logger LOG = LoggerFactory.getLogger(ItemUpdater.class);
	private static final int ONE_MIN = 60000;

	@Autowired
	private GW2Service gw2Service;

	@Scheduled(fixedDelay = ONE_MIN * 15, initialDelay = ONE_MIN)
	public void updateItems() {
		LOG.info("Scheduled run: " + new Date());
		try {
			gw2Service.updateListings(new ItemSearchParameters());
		} catch (Exception e) {
			LOG.error("Error with scheduled job", e);
		}
	}
	
	@Scheduled(fixedDelay = ONE_MIN * 15)
	public void updateSellAverages() {
		LOG.info("Started updating sell averages: " + new Date());
		try {
			gw2Service.updateSellAverages();
		} catch (Exception e) {
			LOG.error("Error with updating ", e);
		}
		LOG.info("Finished updating sell averages: " + new Date());
	}
	
	@Scheduled(fixedDelay = ONE_MIN * 15)
	public void updateBuyAverages() {
		LOG.info("Started updating buy averages: " + new Date());
		try {
			gw2Service.updateBuyAverages();
		} catch (Exception e) {
			LOG.error("Error with updating ", e);
		}
		LOG.info("Finished updating buy averages: " + new Date());
	}
}
