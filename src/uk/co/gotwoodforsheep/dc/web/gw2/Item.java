package uk.co.gotwoodforsheep.dc.web.gw2;

public class Item {

	private Integer dataId;
	private Integer typeId;
	private String name;
	private String description;
	private Integer level;
	private Integer rarity;
	private Integer vendorPrice;
	private String imgUrl;
	private String rarityWord;

	public Integer getDataId() {
		return dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getRarity() {
		return rarity;
	}

	public void setRarity(Integer rarity) {
		this.rarity = rarity;
	}

	public Integer getVendorPrice() {
		return vendorPrice;
	}

	public void setVendorPrice(Integer vendorPrice) {
		this.vendorPrice = vendorPrice;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getRarityWord() {
		return rarityWord;
	}

	public void setRarityWord(String rarityWord) {
		this.rarityWord = rarityWord;
	}

}
