/**
 * 
 */
package uk.co.gotwoodforsheep.dc.web.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Sean
 * 
 */
@Controller
@RequestMapping("file/")
public class EditFileController {

	@RequestMapping(value = "find.html", method = RequestMethod.GET)
	public String showFileSelect() {
		return "file/select";
	}

	@RequestMapping(value = "find.html", method = RequestMethod.POST)
	public String fileSelectPost(@RequestParam("file") String file, ModelMap modelMap) throws IOException {
		File f = new File(file);
		if (!f.exists() || f.isDirectory()) {
			return "file/select";
		} else {
			modelMap.put("fileText", readFile(f));
			modelMap.put("file", file);
			return "file/edit";
		}
	}
	
	@RequestMapping(value = "find.html", method = RequestMethod.POST, params = "html")
	public String fileEditPost(@RequestParam("file") String file,@RequestParam("html") String html, ModelMap modelMap) throws IOException {
		File f = new File(file);
		FileWriter fileWriter = new FileWriter(f);
		fileWriter.write(html);
		fileWriter.close();
		modelMap.put("fileText", html);
		return "file/success";
	}
	
	private String readFile( File file ) throws IOException {
	    BufferedReader reader = new BufferedReader( new FileReader (file));
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    String         ls = System.getProperty("line.separator");

	    while( ( line = reader.readLine() ) != null ) {
	        stringBuilder.append( line );
	        stringBuilder.append( ls );
	    }

	    return stringBuilder.toString();
	}
}
