package uk.co.gotwoodforsheep.dc.web.controller;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import uk.co.gotwoodforsheep.dc.web.service.GoogleService;

@Controller
public class GoogleOAuthController {

	@Autowired
	private GoogleService googleService;
	
	@ResponseBody
	@RequestMapping("/google")
	public String checkGoogle() {
		String s = googleService.getDetails();
		System.out.println(s);
		return s;
	}
	
	@ResponseBody
	@RequestMapping("/session")
	public String getSession(HttpServletRequest httpServletRequest ) {
		Enumeration<String> strings = httpServletRequest.getSession().getAttributeNames();
		while (strings.hasMoreElements()) {
			String key = strings.nextElement();
			System.out.println(key + " : " + httpServletRequest.getSession().getAttribute(key));
		}
		
		
		return "" + httpServletRequest.getSession().getAttributeNames();
	}
}
