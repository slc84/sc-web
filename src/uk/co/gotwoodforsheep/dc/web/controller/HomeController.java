/**
 * 
 */
package uk.co.gotwoodforsheep.dc.web.controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import uk.co.gotwoodforsheep.dc.web.dao.ApplicationDAO;

/**
 * @author Sean
 * 
 */
@Controller
@RequestMapping({"/", "welcome.html"})
public class HomeController {

	@Autowired
	private ApplicationDAO testDAO;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getHome(ModelMap map) throws SQLException {
		return "home";
	}
}
