/**
 * 
 */
package uk.co.gotwoodforsheep.dc.web.bean.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author Sean
 *
 */
public class UserDetails extends org.springframework.security.core.userdetails.User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1250127444673486665L;
	
	public UserDetails(String username, String password, boolean enabled, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, true, true, true, authorities);
	}
	
}
