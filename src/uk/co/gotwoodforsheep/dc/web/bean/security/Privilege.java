/**
 * 
 */
package uk.co.gotwoodforsheep.dc.web.bean.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author Sean
 *
 */
public class Privilege implements GrantedAuthority {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2552976950015386414L;
	
	private Long id;
	private String authority;
	private String description;
	
	public Privilege(Long id, String authority, String description) {
		this.id = id;
		this.authority = authority;
		this.description = description;
	}
	
	@Override
	public String getAuthority() {
		return authority;
	}

	public String getDescription() {
		return description;
	}
	
	public Long getId() {
		return id;
	}
}
