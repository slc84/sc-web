package uk.co.gotwoodforsheep.dc.web.security.util;

import org.springframework.security.crypto.password.StandardPasswordEncoder;

public class PasswordGenerator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		StandardPasswordEncoder encoder = new StandardPasswordEncoder();
		String s = encoder.encode("password");
		System.out.println("[" + s + "]");
	}

}
