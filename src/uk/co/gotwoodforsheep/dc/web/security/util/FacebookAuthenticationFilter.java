package uk.co.gotwoodforsheep.dc.web.security.util;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.util.TextEscapeUtils;

import uk.co.gotwoodforsheep.dc.web.service.FacebookService;

public class FacebookAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	protected FacebookAuthenticationFilter() {
		super("/oauth2/facebook");
	}

	@Autowired
	private FacebookService facebookService;

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		SecurityContextHolder.clearContext();
		String output = facebookService.getDetails();
		System.out.println(output);
		UserDetails userDetails = new User("bob", "none", Arrays.asList(new GrantedAuthorityImpl("user")));
		PreAuthenticatedAuthenticationToken authentication = new PreAuthenticatedAuthenticationToken(userDetails,
				userDetails, Arrays.asList(new GrantedAuthorityImpl("user")));
		if (request.getSession(false) != null || getAllowSessionCreation()) {
			request.getSession().setAttribute("SPRING_SECURITY_LAST_USERNAME_KEY",
					TextEscapeUtils.escapeEntities("bob"));
		}

		// Allow subclasses to set the "details" property
		setDetails(request, authentication);

		return this.getAuthenticationManager().authenticate(authentication);
	}

	protected void setDetails(HttpServletRequest request, PreAuthenticatedAuthenticationToken authRequest) {
		authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
	}
}
