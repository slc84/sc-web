
CREATE TABLE users (
  id BIGINT NOT NULL AUTO_INCREMENT,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(256) NOT NULL,
  enabled BOOLEAN NOT NULL,
  PRIMARY KEY (id)
)
--
CREATE TABLE user_groups (
  user_id BIGINT NOT NULL,
  group_id BIGINT NOT NULL,
  PRIMARY KEY (user_id,group_id)
)
--
CREATE TABLE groups (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(256) NOT NULL,
  PRIMARY KEY (id)
)
--
CREATE TABLE group_privileges (
  group_id BIGINT NOT NULL,
  privilege_id BIGINT NOT NULL,
  PRIMARY KEY (group_id,privilege_id)
)
--
CREATE TABLE privileges (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(256) NOT NULL,
  PRIMARY KEY (id)
)

