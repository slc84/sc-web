insert into users values (1,'bob', 'e0c829f3901adf2a242cf4838a133a12fee21db08990885157f03038177ef0bf096d4bfbd078adf0', 1);
--
insert into groups values (1,'Administrators', 'This is the Admin role, only admins have this.');
--
insert into privileges values (1,'admin', 'This is the Admin role, only admins have this.');
--
insert into privileges values (2,'user', 'User Role, gives access to the system.');
--
insert into user_groups values (1, 1);
--
insert into group_privileges values (1, 1);
--
insert into group_privileges values (1, 2);