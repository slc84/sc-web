<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>GW2 Buy Buy</title>
</head>
<body>
<h1>GW2 Buys</h1>

<c:choose>
	<c:when test="${not empty buys}">
		<table>
			<tr>
				<th>name</th>
				<th>level</th>
				<th>seller price</th>
				<th>buyer price</th>
				<th>vendor price</th>
				<th>TP Diff</th>
				<th>Vendor Diff</th>
				<th>Date</th>
			</tr>
			<c:forEach items="${buys}" var="item">
			<tr>
				<td>${item.name}</td>
				<td>${item.level}</td>
				<td>${item.sellerPrice}</td>
				<td>${item.buyerPrice}</td>
				<td>${item.vendorPrice}</td>
				<td>${item.tpDiff}</td>
				<td>${item.vendorDiff}</td>
				<td>${item.date}</td>
			</tr>
			</c:forEach>
		</table>
	</c:when>
	<c:otherwise>
		<p>No definite items!</p>
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${not empty lowSellingItems}">
		<table>
			<tr>
				<th>Name</th>
				<th>Level</th>
				<th>Seller Price</th>
				<th>Seller Count</th>
				<th>Buyer Price</th>
				<th>AVG Buyer Price</th>
				<th>Vendor Price</th>
				<th>Avg Buy Vs. Current Sell</th>
				<th>Date</th>
			</tr>
			<c:forEach items="${lowSellingItems}" var="item">
			<tr>
				<td>${item.name}</td>
				<td>${item.level}</td>
				<td>${item.sellPrice}</td>
				<td>${item.sellCount}</td>
				<td>${item.buyPrice}</td>
				<td>${item.averageBuyPrice}</td>
				<td>${item.vendorPrice}</td>
				<td>${item.avgBuySellDiff}</td>
				<td>${item.date}</td>
			</tr>
			</c:forEach>
		</table>
	</c:when>
	<c:otherwise>
		<p>No Low Selling items</p>
	</c:otherwise>
</c:choose>
</body>
</html>