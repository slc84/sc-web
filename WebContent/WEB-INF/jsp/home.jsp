<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Spring 3.0 MVC Series: Hello World - ViralPatel.net</title>
    <c:url value="/js/tiny_mce/tiny_mce.js" var="tinymce"/>
    <script type="text/javascript" src="${tinymce}"></script>
	<script type="text/javascript">
	tinymce.init({
	    selector: "textarea"
	 });
	</script>
</head>
<body>
<form method="post">
    <textarea>bob</textarea>
</form>

</body>
</html>